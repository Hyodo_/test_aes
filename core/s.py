from django.utils.deprecation import MiddlewareMixin

class MyMiddleware(MiddlewareMixin):
	def process_response(self, request, response):
		response['access-control-allow-headers'] = 'accept, accept-encoding, authorization, content-type, dnt, origin, user-agent, x-csrftoken, x-requested-with, Cache-Control'
		response['access-control-allow-methods'] = 'DELETE, GET, OPTIONS, PATCH, POST, PUT'
		#response['access-control-allow-origin'] = 'http://127.0.0.1:4200'
		response['access-control-allow-origin'] = '*'
		response['access-control-max-age'] = 86400
		response['Access-Control-Allow-Credentials'] = 'true'
		return response
	def process_request(self, request):
		setattr(request, '_dont_enforce_csrf_checks', True)