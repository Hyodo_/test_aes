from django.shortcuts import render
import json
from django.http import JsonResponse, HttpResponse
import hashlib
from Crypto.Cipher import AES
from Crypto import Random
import base64
# Create your views here.

class AESCipher(object):

    def __init__(self, key): 
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


def encryptdata(request):
	password = request.GET.get('password')
	data = request.GET.get('data')
	cipher = AESCipher(password)
	result = cipher.encrypt(data)
	return HttpResponse(result)

def decryptdata(request):
	password = request.GET.get('password')
	data = request.GET.get('data')
	cipher = AESCipher(password)
	result = cipher.decrypt(data)
	return HttpResponse(result)